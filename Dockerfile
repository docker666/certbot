FROM alpine

RUN apk add --update --no-cache \
    openssl \
    ca-certificates \
    py-dnspython \
    tzdata \
    py3-pip

RUN pip install certbot-dns-route53 certbot-dns-godaddy
